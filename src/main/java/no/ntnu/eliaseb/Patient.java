package no.ntnu.eliaseb;

import java.io.Serializable;
import java.util.Objects;

/**
 * This class represents a Patient in the registry of a hospital.
 * It has fields for first name, last name,
 * social security number, diagnosis and general practitioner.
 * Patients are considered equal if they have the same social security number.
 */
public class Patient implements Serializable {
    private String firstName;
    private String lastName;
    private final String socialSecurityNumber;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Creates a person with first name, last name and social security number.
     * @param firstName The first name of the Patient
     * @param lastName The last name of the Patient
     * @param socialSecurityNumber The social security number of the Patient.
     * @param diagnosis The social security number of the Patient.
     * @param generalPractitioner The social security number of the Patient.
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber, String diagnosis, String generalPractitioner){
        if (invalidInput(firstName, lastName, socialSecurityNumber)){
            throw new IllegalArgumentException();
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Checks if the data of the patient is valid or not.
     * @param firstName The first name of the Patient
     * @param lastName The last name of the Patient
     * @param socialSecurityNumber The social security number of the Patient.
     * @return true if any of the fields have invalid data, false if the data is valid.
     */
    private boolean invalidInput(String firstName, String lastName, String socialSecurityNumber){
        boolean invalidFirstName = firstName == null || firstName.equals("");
        boolean invalidLastName = lastName == null || lastName.equals("");
        boolean invalidSSN = socialSecurityNumber == null || socialSecurityNumber.length() != 11 || !socialSecurityNumber.matches("^[0-9]+");
        return invalidSSN  || invalidFirstName || invalidLastName;
    }

    /**
     * Gets the first name of the Patient.
     * @return The first name of the Patient
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of the Patient
     * @param firstName the new first name of the Patient
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * gets the last name of the Patient.
     * @return the last name of the Patient
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * sets the last name of the Patient
     * @param lastName the new last name of the Patient
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * gets the social security number of the Patient
     * @return the social security number of the Patient
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * gets the Patient's full name.
     * @return The full name of the Patient
     */
    public String getFullName(){
        return lastName + ", " + firstName;
    }

    /**
     * gets the patients diagnosis
     * @return the patient's diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * sets the patients diagnosis
     * @param diagnosis the patients diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Gets the patient's general practitioner.
     * @return the patient's general practioner.
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * sets the patient's general practitioner
     * @param generalPractitioner the patients new general practitioner.
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Checks if the Patient is equal to another Object.
     * @param o the object we are checking against.
     * @return True if they are the same person, false if they are not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    /**
     * Generates a hashcode for the two objects
     * @return the generated hashcode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Creates the tostring for the person, showing all fields.
     * @return The tostring of the person
     */
    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}
