package no.ntnu.eliaseb;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * This class is an factory for creating graphic user interface objects.
 * It is not used since the the scenes are made using fxml files.
 * This class consists of a single static method that creates the object.
 */
public class ObjectFactory {

    /**
     * Creates a graphic object based on the type given
     * @param type The type of the object that should be created.
     * @return an object of the chosen type if the chosen type is among the implemented types, null if not.
     */
    public static Object createObject(String type){
        switch(type) {
            case "MenuItem":
                return new MenuItem();
            case "Menu":
                return new Menu();
            case "MenuBar":
                return new MenuBar();
            case "Toolbar":
                return new ToolBar();
            case "TableView":
                return new TableView();
            case "Button":
                return new Button();
            case "HBox":
                return new HBox();
            case "Vbox":
                return new VBox();
            case "BorderPane":
                return new BorderPane();
            default:
                return null;
        }
    }
}
