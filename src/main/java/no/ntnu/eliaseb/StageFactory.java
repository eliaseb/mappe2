package no.ntnu.eliaseb;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.eliaseb.controllers.AddEditController;
import no.ntnu.eliaseb.controllers.Controller;

import java.io.IOException;

/**
 * This class is an factory for creating stages.
 * It consists only of an single method that creates the stage.
 */
public  class StageFactory {
    /**
     * Creates a stage based on the controller and the relative path of an FXML file.
     * @param relativePathName the relative path of the fxml file.
     * @param controller The controller for the fxml file.
     * @return the Stage crated, null if something goes wrong.
     */
    public static Stage createStage(String relativePathName, Controller controller){
        try {
            FXMLLoader loader = new FXMLLoader(controller.getClass().getResource(relativePathName));
            Stage newStage = new Stage();
            newStage.initModality(Modality.APPLICATION_MODAL);
            loader.setController(controller);
            newStage.setScene(new Scene(loader.load()));
            return newStage;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
