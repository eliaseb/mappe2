package no.ntnu.eliaseb;

import javafx.application.Application;
import javafx.stage.Stage;
import no.ntnu.eliaseb.controllers.MainController;

import java.io.IOException;

/**
 * This class is the class that initializes and stores.
 * Primary stage is it's only field.
 */
public class PatientApplication extends Application {
    private Stage primaryStage;

    /**
     * Get's the primary stage of the application.
     * @return the primary stage of the application.
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Set's the primary stage of the application.
     * @param primaryStage the new stage of the application.
     */
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    /**
     * This method is run when the this classed is launched.
     * @param stage The stage that is the primary stage.
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        PatientRegistry patients = new PatientRegistry();
        setPrimaryStage(stage);
        MainController controller = new MainController(patients);
        primaryStage.setScene(StageFactory.createStage("/main.fxml", controller).getScene());
        primaryStage.show();
    }
}
