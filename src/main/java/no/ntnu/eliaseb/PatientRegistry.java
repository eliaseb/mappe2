package no.ntnu.eliaseb;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 * This class represents a registry of patients in a hospital.
 * It has one arraylist of patients.
 * It has methods for adding and removing patients,
 * and for importing and exporting the list of patients.
 */
public class PatientRegistry {
    private ArrayList<Patient> patients;

    /**
     *Creates a new patient registry with an empty patient list.
     */
    public PatientRegistry() {
        patients = new ArrayList<>();
    }

    /**
     * Creates and adds a new patient to the patient list.
     * @param firstName The first name of the new patient
     * @param lastName The last name of the new patient
     * @param socialSecurityNumber the social security number of the last patient
     * @param diagnosis the diagnosis of the new patient.
     * @param generalPractitioner the general practitioner of the new patient.
     * @return True if the patient was added successfully, false if it was not.
     */
    public boolean addPatient(String firstName, String lastName, String socialSecurityNumber, String diagnosis, String generalPractitioner){
        try{
            Patient patient = new Patient(firstName, lastName, socialSecurityNumber, diagnosis, generalPractitioner);
            if(patients.contains(patient)){
                return false;
            } else{
                patients.add(patient);
                return true;
            }
        } catch (IllegalArgumentException e){
            return false;
        }
    }

    /**
     * Finds and returns the patient with a certain social security number.
     * @param socialSecurityNumber The social security number of the patient that should be found.
     * @return the patient if it is found, null if no not.
     */
    public Patient getPatient(String socialSecurityNumber){
        return patients.stream().filter(patient -> patient.getSocialSecurityNumber().equals(socialSecurityNumber)).findFirst().orElse(null);
    }

    /**
     * Checks if the patient registry contains a patient with the correct social security number
     * @param socialSecurityNumber The social security number of the patient that is being looked for.
     * @return true if the patient is found, false if it is not.
     */
    public boolean containsPatient(String socialSecurityNumber){
        return getPatient(socialSecurityNumber) != null;
    }


    /**
     * Deletes a patient from the registry.
     * @param patient The patient that are being deleted.
     * @return true if the patient is deleted, false if it is not.
     */
    public boolean deletePatient(Patient patient){
        return patients.remove(patient);
    }

    /**
     * get's the arrayList of patients.
     * @return The arrayList of patients.
     */
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Set's the arrayList of patients
     * @param patients the new arraylist of patients.
     */
    public void setPatients(ArrayList<Patient> patients) {
        this.patients = patients;
    }

    /**
     * Saves the data in the registry to the specified file.
     * @param file the file that is being saved to.
     * @return true if the data was saved successfully, false if not.
     */
    public boolean saveToFile(File file) {
        try {
            FileOutputStream fileOut = new FileOutputStream(file);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(patients);
            objectOut.close();
            return true;
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }


    /**
     * Loads the data from a file, and set's it as the patients arrayList.
     * @param file The file the data is being imported from.
     * @return true if the data was imported successfully, false if it was not.
     */
    public boolean loadFromFile(File file) {
        try {
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            patients = (ArrayList<Patient>) objectIn.readObject();
            objectIn.close();
            return true;
        } catch (Exception ex){
            //ex.printStackTrace();
            return false;
        }
    }


    /**
     * Checks if the patientRegistry is equal to the object passed
     * @param o The object that is being checked against.
     * @return true if the object is the same patientRegistry, false if it is not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientRegistry that = (PatientRegistry) o;
        return Objects.equals(patients, that.patients);
    }

    /**
     * Generates a hashCode for the patient registry.
     * @return The hashcode generated.
     */
    @Override
    public int hashCode() {
        return Objects.hash(patients);
    }

    /**
     * creates a string showing the values of the patients.
     * @return The string showing the values of the patients.
     */
    @Override
    public String toString() {
        return "PatientRegister{" +
                "patients=" + patients +
                '}';
    }
}
