package no.ntnu.eliaseb.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.eliaseb.Patient;
import no.ntnu.eliaseb.PatientRegistry;
import no.ntnu.eliaseb.controllers.Controller;

import java.util.Optional;

/**
 * This class represents the controller for the add / edit patient window.
 * It inherits from the Controller class.
 * It has one fields for the patient being edited.
 */
public class AddEditController extends Controller {
    private Patient patient;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField socialSecurityNumberTextField;

    @FXML
    private Button cancelButton;

    @FXML
    private Button submitButton;

    /**
     * Creates an AddEditController without a patient.
     * Is used when creating a new patient.
     * @param patients the patient registry the patient should be added to.
     */
    public AddEditController(PatientRegistry patients){
        super(patients);
    }

    /**
     * Creates an AddEditController with a patient.
     * Is used when editing an existing patient
     * @param patient The patient being edited
     * @param patients The patient registry of the patient being edited.
     */
    public AddEditController(Patient patient, PatientRegistry patients){
        super(patients);
        this.patient = patient;
    }

    /**
     * Closes the stage of addEditController.
     * @param event The event that triggered this method.
     */
    void closeStage(ActionEvent event){
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    /**
     * Initializes the initial values of the text fields if a patient is being edited.
     * This method is called immediately after the controller is created.
     */
    @FXML
    void initialize(){
        //Checks if a patient is being added or edited.
        if (patient != null){
            //A patient is being edited,  so the fields are set with the patient's values.
            firstNameTextField.setText(patient.getFirstName());
            lastNameTextField.setText(patient.getLastName());
            socialSecurityNumberTextField.setText(patient.getSocialSecurityNumber());
            //Since a patient can't change it's social security number, the field for changing it is disabled.
            socialSecurityNumberTextField.setDisable(true);
        }
    }

    /**
     * Closes the stage.
     * This method is called when the user clicks the cancel button.
     * @param event The event that triggered this method.
     */
    @FXML
    void onCancelButton(ActionEvent event) {
        closeStage(event);
    }

    /**
     * This method checks if the fields are valid.
     * If they are not, the invalid fields will be marked with a red border.
     * If the fields are valid the patient will be created or edited depending on how the stage was opened
     * This method is called when the user clicks the submit button.
     * @param event The event that triggered this method.
     */
    @FXML
    void onSubmitButton(ActionEvent event) {
        boolean illegalInput = false;
        //Checks if the first name is valid
        if(firstNameTextField.getText().equals("")){
            firstNameTextField.setPromptText("Field Required");
            firstNameTextField.setStyle("-fx-border-color: red");
            illegalInput = true;
        }
        //Checks if the last is valid.
        if(lastNameTextField.getText().equals("")){
            lastNameTextField.setPromptText("Field Required");
            lastNameTextField.setStyle("-fx-border-color: red");
            illegalInput = true;
        }
        //Checks if the social security number is valid.
        if(!socialSecurityNumberTextField.getText().matches("^[0-9]+")){
            //The social security number contains a symbol that is not a digit.
            socialSecurityNumberTextField.setText("");
            socialSecurityNumberTextField.setPromptText("Must be a number");
            socialSecurityNumberTextField.setStyle("-fx-border-color: red");
            illegalInput = true;
        } else if(socialSecurityNumberTextField.getText().length()!=11){
            //The number is not 11 digits long.
            socialSecurityNumberTextField.setText("");
            socialSecurityNumberTextField.setPromptText("must have 11 digits");
            socialSecurityNumberTextField.setStyle("-fx-border-color: red");
            illegalInput = true;
        }

        if(!illegalInput){
            //The input fields are legal
            if(patient == null){
                //We are creating a new patient
                if(patientRegistry.containsPatient(socialSecurityNumberTextField.getText())) {
                    //The social security number is already in use
                    socialSecurityNumberTextField.setText("");
                    socialSecurityNumberTextField.setPromptText("Number already in use");
                    socialSecurityNumberTextField.setStyle("-fx-border-color: red");
                } else{
                    //The social security number is not in use.
                    if (patientRegistry.addPatient(firstNameTextField.getText(), lastNameTextField.getText(), socialSecurityNumberTextField.getText(), null, "")){
                        //Patient was added succesfully.
                        closeStage(event);
                    } else {
                        //Something went wrong during the adding proccess.
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setHeaderText("The patient could not be added properly.");
                        Optional<ButtonType> result = alert.showAndWait();
                    }
                }
            } else{
                //We are editing a patient
                patient.setFirstName(firstNameTextField.getText());
                patient.setLastName(lastNameTextField.getText());
                closeStage(event);
            }
        }



    }

}
