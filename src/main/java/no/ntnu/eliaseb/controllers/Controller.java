package no.ntnu.eliaseb.controllers;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import no.ntnu.eliaseb.PatientRegistry;

/**
 * This method represents an abstract controller.
 * It has one field for the patient registry.
 * This class is a superclass for the controllers in this product.
 */
public abstract class Controller {
    protected PatientRegistry patientRegistry;

    /**
     * Creates a new Controller object.
     * @param patientRegistry The patient registry of the controller object.
     */
    public Controller(PatientRegistry patientRegistry) {
        this.patientRegistry = patientRegistry;
    }


}
