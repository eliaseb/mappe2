package no.ntnu.eliaseb.controllers;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import no.ntnu.eliaseb.Patient;
import no.ntnu.eliaseb.PatientRegistry;
import no.ntnu.eliaseb.StageFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * This class represents the controller for the main application
 * It inherits from the Controller class.
 * It has two fields: an observable list of patients, and a file chooser.
 */
public class MainController extends Controller {
    ObservableList<Patient> observablePatients;
    FileChooser fileChooser;

    @FXML
    private Button addPatientButton;

    @FXML
    private ImageView addIcon;

    @FXML
    private Button removePatientButton;

    @FXML
    private ImageView removeIcon;

    @FXML
    private Button editPatientButton;

    @FXML
    private ImageView editIcon;

    @FXML
    private MenuBar mainMenuBar;

    @FXML
    private Menu fileMenu;

    @FXML
    private MenuItem importMenuItem;

    @FXML
    private MenuItem exportMenuItem;

    @FXML
    private MenuItem exitMenuItem;

    @FXML
    private Menu editMenu;

    @FXML
    private MenuItem addPatientMenuItem;

    @FXML
    private MenuItem editPatientMenuItem;

    @FXML
    private MenuItem removePatientMenuItem;

    @FXML
    private Menu helpMenu;

    @FXML
    private MenuItem aboutMenuItem;

    @FXML
    private TableView<Patient> patientTable;

    @FXML
    private TableColumn<Patient, String> firstNameColumn;

    @FXML
    private TableColumn<Patient, String> lastNameColumn;

    @FXML
    private TableColumn<Patient,String> socialSecurityNumberColumn;

    /**
     * Creates a new MainController object.
     * @param patientRegistry The patient registry of the controller object.
     */

    public MainController(PatientRegistry patientRegistry){
        super(patientRegistry);
        fileChooser = new FileChooser();
        //Forces the user to only import .csv files, and automatically makes files exported .csv.
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Patient files",  "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

    }

    /**
     * Updates the tableview patientTable
     */
    public void updateTable(){
        observablePatients = FXCollections.observableArrayList(patientRegistry.getPatients());
        patientTable.setItems(observablePatients);
    }

    /**
     * Initializes the table view object.
     * This method is called immediately after the main controller is created.
     */
    @FXML
    void initialize(){
        updateTable();
        firstNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirstName()));
        lastNameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLastName()));
        socialSecurityNumberColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSocialSecurityNumber()));
    }

    /**
     * Get's the window that the controller is responsible for.
     * @return The window the controller is responsible for.
     */
    public Window getWindow(){
        return patientTable.getScene().getWindow();
    }

    /**
     * Shows the user an alert with information about the product.
     * Is called when user clicks the About menu item
     * @param event The event that triggered this method.
     */
    @FXML
    void onAboutMenuItem(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("About the patient register application.");
        alert.setContentText("This app was created by a NTNU student.");
        Optional<ButtonType> result = alert.showAndWait();
    }

    /**
     * Creates a new stage, where the user can add a new patient
     * This method is called when user clicks on the add patient button.
     * @param event The event that triggered this method.
     */
    @FXML
    void onAddPatientButton(ActionEvent event) {
        AddEditController controller = new AddEditController(patientRegistry);
        Stage newStage = StageFactory.createStage("/addEdit.fxml", controller);
        //Checks if stage was created
        if(newStage != null){
            newStage.setOnHidden(e -> updateTable());
            newStage.show();
        }
    }

    /**
     * Creates a new stage, where the user can add a new patient
     * This method is called when user clicks on the add patient menu item.
     * @param event The event that triggered this method.
     */
    @FXML
    void onAddPatientMenuItem(ActionEvent event) {
        onAddPatientButton(event);
    }

    /**
     * creates a new stage that let's the user edit the selected patient.
     * Does nothing if no patient is selected.
     * This method is called when the user clicks the edit patient button.
     * @param event The event that triggered this method.
     */
    @FXML
    void onEditPatientButton(ActionEvent event) {
        Patient selectedPatient = patientTable.getSelectionModel().getSelectedItem();
        //Checks if a patient is selected.
        if (selectedPatient != null){
            AddEditController controller = new AddEditController(selectedPatient, patientRegistry);
            Stage newStage = StageFactory.createStage("/addEdit.fxml", controller);
            //Checks if stage was created
            if(newStage != null){
                newStage.setOnHidden(e -> patientTable.refresh());
                newStage.show();
            }
        }
    }

    /**
     * creates a new stage that let's the user edit the selected patient.
     * Does nothing if no patient is selected.
     * This method is called when the user clicks the edit patient menu item.
     * @param event The event that triggered this method.
     */
    @FXML
    void onEditPatientMenuItem(ActionEvent event) {
        onEditPatientButton(event);
    }

    /**
     * Closes the window.
     * this method is called when the user clicks the exit menu item
     * @param event The event that triggered this method.
     */
    @FXML
    void onExitMenuItem(ActionEvent event) {
        getWindow().hide();
    }

    /**
     * Opens a file choose dialog box that let's the user choose location to export data to.
     * A new file will be created if the user chooses a file that doesn't exist
     * The dialog box will give the user a warning if the user considers overwriting an existing file.
     * Once the file has been chosen, the data will be exported the file.
     * A alert will show whether the export was successful or not.
     * This method is called when the user clicks the export menu item
     * @param event The event that triggered this method.
     */
    @FXML
    void onExportMenuItem(ActionEvent event) {
        fileChooser.setTitle("Choose location to save file to.");
        File file = fileChooser.showSaveDialog(getWindow());
        //Checks if file was chosen.
        if(file != null){
            //Checks if file was saved successfully
            if(patientRegistry.saveToFile(file)){
                //File was saved successfully
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Success");
                alert.setHeaderText("The File exported successfully.");
                Optional<ButtonType> result = alert.showAndWait();
            } else{
                //File was not saved successfully
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Unexpected exception");
                alert.setHeaderText("The chosen file could not be read or is not valid.");
                alert.setContentText("Please select a different file.");
                Optional<ButtonType> result = alert.showAndWait();
            }
        }
    }

    /**
     * Opens a file choose dialog box that let's the user choose location to import data from.
     * Once the file has been chosen, data will be imported the file.
     * A alert will show if the file could not be read.
     * This method is called when the user clicks the import menu item
     * @param event The event that triggered this method.
     */
    @FXML
    void onImportMenuItem(ActionEvent event) {
        fileChooser.setTitle("Open PatientRegistry File");
        File file = fileChooser.showOpenDialog(getWindow());
        //Checks if file was chosen
        if(file != null){
            //Checks if patients were successfully imported
            if(patientRegistry.loadFromFile(file)){
                //Patients were successfully imported
                updateTable();
            } else{
                //Patients were not succesfully imported.
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Unexpected exception");
                alert.setHeaderText("The chosen file could not be read or is invalid.");
                alert.setContentText("Please select a different file.");
                Optional<ButtonType> result = alert.showAndWait();
            }
        }
    }

    /**
     * This method removes the selected patient, if any.
     * This method is called when the user clicks the remove patient button.
     * @param event The event that triggered this method.
     */
    @FXML
    void onRemovePatientButton(ActionEvent event) {
        Patient selectedPatient = patientTable.getSelectionModel().getSelectedItem();
        //Checks if a patient is selected
        if (selectedPatient != null){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Comfirm Delete?");
            alert.setHeaderText("Are you sure you wish to remove patient?");
            alert.setContentText("This action cannot be undone.");
            Optional<ButtonType> result = alert.showAndWait();
            //Confirms if the user wants to delete patient
            if(result.get() == ButtonType.OK){
                patientRegistry.deletePatient(selectedPatient);
                updateTable();
            }
        }


    }

    /**
     * This method removes the selected patient, if any.
     * This method is called when the user clicks the remove patient menu item.
     * @param event The event that triggered this method.
     */
    @FXML
    void onRemovePatientMenuItem(ActionEvent event) {
        onRemovePatientButton(event);
    }
}
