import javafx.application.Application;
import no.ntnu.eliaseb.PatientApplication;

/**
 * This class launches the patient application class.
 */
public class Launcher {
    public static void main(String[] args) {
        Application.launch(PatientApplication.class, args);
    }

}
