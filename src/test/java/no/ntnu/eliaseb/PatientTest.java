package no.ntnu.eliaseb;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PatientTest {
    private Patient patient;

    @BeforeEach
    public void setUp(){
        patient = new Patient("Ole", "Nordmann", "23040050623", "Mild back pain.", "Ove Ralt");
    }

    @Test
    public void firstNameTest(){
        assertEquals("Ole", patient.getFirstName());

        Boolean emptyStringFirstName = false;
        try {new Patient("", "Nordmann", "23040050623", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            emptyStringFirstName = true;
        }
        assertTrue(emptyStringFirstName);

        Boolean nullFirstName = false;
        try {new Patient(null, "Nordmann", "23040050623", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            nullFirstName = true;
        }
        assertTrue(nullFirstName);
    }

    @Test
    public void lastNameTest(){
        assertEquals("Nordmann", patient.getLastName());

        Boolean emptyStringLastName = false;
        try {new Patient("", "Nordmann", "23040050623", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            emptyStringLastName = true;
        }
        assertTrue(emptyStringLastName);

        Boolean nullLastName = false;
        try {new Patient(null, "Nordmann", "23040050623", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            nullLastName = true;
        }
        assertTrue(nullLastName);
    }

    @Test
    public void socialSecurityNumberTest(){
        assertEquals("23040050623", patient.getSocialSecurityNumber());

        Boolean emptyStringSSN = false;
        try {new Patient("", "Nordmann", "", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            emptyStringSSN = true;
        }
        assertTrue(emptyStringSSN);

        Boolean nullSSN = false;
        try {new Patient(null, "Nordmann", null, "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            nullSSN = true;
        }
        assertTrue(nullSSN);

        Boolean tooLongSSN = false;
        try {new Patient("", "Nordmann", "2304005062323", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            tooLongSSN = true;
        }
        assertTrue(tooLongSSN);

        Boolean tooShortSSN = false;
        try {new Patient(null, "Nordmann", "230450623", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            tooShortSSN = true;
        }
        assertTrue(tooShortSSN);

        Boolean notANumberSSN = false;
        try {new Patient(null, "Nordmann", "230450sdf", "Mild back pain.", "Ove Ralt");
        } catch (IllegalArgumentException e){
            notANumberSSN = true;
        }
        assertTrue(notANumberSSN);
    }

    @Test
    public void diagnosisTest(){
        assertEquals("Mild back pain.", patient.getDiagnosis());

    }

    @Test
    public void generalPractitionerTest(){
        assertEquals("Ove Ralt", patient.getGeneralPractitioner());
    }


}
