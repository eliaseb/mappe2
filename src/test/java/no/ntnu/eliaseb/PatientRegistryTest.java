package no.ntnu.eliaseb;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class PatientRegistryTest {
    private PatientRegistry patientRegistry;

    @BeforeEach
    public void setUp(){
        patientRegistry = new PatientRegistry();
        patientRegistry.addPatient("Ole", "Nordmann", "23040050623", "Mild back pain.", "Ove Ralt");
        patientRegistry.addPatient("Bob", "Trønder", "22040050623", "knife wound.", "Kari knutsen");
        patientRegistry.addPatient("Alex", "Bergenser", "25040050623", "Major blood loss.", "Birgitte Brynhild");
    }

    @Test
    public void PatientRegistryPatientsTest(){
        assertTrue(patientRegistry.containsPatient("23040050623"));

        assertFalse(patientRegistry.containsPatient("12312312312"));

        assertTrue(patientRegistry.deletePatient(new Patient("Bob", "Trønder", "22040050623", "knife wound.", "Kari knutsen")));

        assertFalse(patientRegistry.deletePatient(new Patient("Bob", "Trønder", "22040050623", "knife wound.", "Kari knutsen")));

        assertTrue(patientRegistry.getPatients().size() == 2);

    }

    @Test
    public void PatientRegistryreadWriteTest(){
        File file = new File("patients.csv");
        assertTrue(patientRegistry.saveToFile(file));

        PatientRegistry newPatientRegistry = new PatientRegistry();
        assertTrue(newPatientRegistry.loadFromFile(file));

        assertEquals(newPatientRegistry, patientRegistry);

        assertTrue(file.delete());
    }




}
